#!/bin/bash

# Test les exemples de requêtes et de réponses de la spécification de l'API.
# Créer un mock serveur à partir de l'API, puis lui envoie des requêtes
# basées sur les exemples décrits dans les spécifications de l'API.

api=api.yaml
port=8000

# Créer le serveur en arrière plan.
apisprout --port $port --validate-request $api &

# Créer et exécuter le client.
api-spatcher $api http://localhost:$port

# Arrête le serveur en arrière plan.
kill $!
