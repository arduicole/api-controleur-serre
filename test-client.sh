#!/bin/bash

# Exécute un serveur avec tous les chemins décrits
# dans api.yaml. Ce serveur répond aux requêtes

api=api.yaml
port=8000

pub=public.cer
priv=private.key

apisprout --port $port --validate-request $api \
    --https --public-key $pub --private-key $priv
