#!/bin/bash

# Génère la documentation du fichier api.yaml
# dans le fichier api.html

source=api.yaml
destination=api.html

redoc-cli bundle $source -o $destination
