#!/bin/bash

# Génère une paire de clés RSA de 4096 bits et d'une durée donnée.

duree=$(expr 30 * 365)

# Générer la clé privée
openssl genrsa 2048 > private.key

# Générer la clé publique
openssl req -new -x509 -nodes -sha1 -days $duree -key private.key > public.cer
