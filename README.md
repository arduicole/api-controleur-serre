# API du contrôleur de serre
Le document [`api.yaml`] documente l'API du serveur du [contrôleur de serre]. Il suit le format [OpenAPI v3.0](https://www.openapis.org/) et décrit tous les chemins et les méthodes HTTP du serveur et les données à lui envoyer et qu'il renvoie pour chaque chemin et méthode.  
Ce format a été choisi parce qu'il existe de nombreux outils qui, à partir de ce seul document, peuvent générer la documentation de l'API, tester un client ou tester un serveur pour s'assurer qu'ils correspondent à la spécification.

**Ce document est la référence pour la programmation du serveur du [contrôleur de serre] ainsi que celle de [l'application mobile].**

## Ressources
Vous n'êtes absolument pas obligés de consulter les ressources données, mais vous avez besoin de notions de base dans tous ces sujets. Je vous recommande, au contraire, de trouver d'autres ressources plus simples pour apprendre ces ressources, car celles fournies sont des documents officiels qui ne sont pas le meilleur endroit où commencer.

Si vous désirez seulement implémenter l'interface, référez-vous à ces ressources:
- [Codes de statut HTTP](https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html)
- [Méthodes HTTP](https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html)
- [Standard de sérialisation JSON](http://json.org/)

Afin de comprendre et de modifier ce document, allez voir celles-ci:
- [Standard de sérialisation YAML](https://yaml.org/)
- [Standard de sérialisation JSON](http://json.org/)
- [Codes de statut HTTP](https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html)
- [Méthodes HTTP](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods)
- [Standard d'API REST](https://www.w3.org/2001/sw/wiki/REST)
- [Spécifications pour OpenAPI](https://github.com/OAI/OpenAPI-Specification)

## Générer la documentation
Il existe plusieurs options pour générer la documentation de l'API à partir du fichier [`api.yaml`]. L'outil que nous allons utiliser s'appelle [ReDoc cli] et génère toute la documentation dans un seul document HTML que vous pouvez ensuite ouvrir avec un navigateur web.

[ReDoc cli] est disponible à partir de [npm](https://www.npmjs.com/) que vous pouvez télécharcher [ici](https://nodejs.org/en/download/) et vous pouvez le télécharger à l'aide de la commande:
```sh
npm install -g redoc-cli
```

Vous pouvez ensuite directement exécuter la commande ou exécuter le fichier bash fournit:
```sh
redoc-cli bundle api.yaml -o api.html
# ou
bash documenter.sh
```

## Tester un client (ex.: l'application android)
Comme mentionné plus tôt, il existe des outils pour créer un serveur qui va lire le fichier [`api.yaml`] et mimiquer notre API pour nous permettre de tester des clients.

Il existe plusieurs options différentes pour générer la documentation de l'API à partir du fichier [`api.yaml`]. Celle qui est utilisée ici est [apisprout](https://github.com/danielgtaylor/apisprout), mais rien ne vous empêche d'en utiliser d'autres. Je vous recommande [d'installer go](https://golang.org/dl/) pour ensuite pouvoir installer [apisprout]. Pour installer [apisprout] utilisez cette commande:
```sh
go get github.com/danielgtaylor/apisprout
```

Vous avez aussi l'option d'installer directement le programme compilé en allant télécharger la version la plus récente [ici](https://github.com/danielgtaylor/apisprout/releases). Cette façon ne vous oblige pas à installer [go](https://golang.org/).

Par la suite vous n'avez qu'à exécuter le fichier bash fournit pour tester un client. Pour arrêter le serveur, faites `Ctrl+C`
```sh
bash test-client.sh
```

## Tester le serveur (ex.: le serveur du contrôleur de serre)
Nous pouvons aussi nous assurer qu'un serveur correspond bel et bien à notre API.
Pour ce faire nous allons utiliser [api-spatcher] qui envoi des requêtes pour toutes les parties différentes de notre API décrites dans le fichier [`api.yaml`] qu'on va lui fournir.

Il faut tout d'abord installer cet outil. Il est également fait en go, donc vous aurez besoin d'installer go pour l'installer. Ensuite, installez l'outil lui-même à l'aide de cette commande:
```sh
go get gitlab.com/ZakCodes/api-spatcher
```

Vous pouvez ensuite effectuer le test avec cette commande ou en exécutant le test dans le fichier bash fournit. Vous allez cependant sûrement vouloir changé l'adresse du serveur pour celle du vôtre.
```sh
api=api.yaml
serveur=http://localhost:8000

api-spatcher $api $serveur

# ou
bash test-serveur.sh
```

## Tester les exemples de requêtes et de réponses du fichier [`api.yaml`]
Pour s'assurer que les exemples de l'API correspondent aux schémas, vous pouvez aussi le tester à l'aide du fichier bash fournit:
```sh
bash test-api.sh
```

<!-- liens récurrents -->
[`api.yaml`]: api.yaml
[contrôleur de serre]: https://gitlab.com/arduicole/controleur-serre
[l'application mobile]: https://gitlab.com/arduicole/controleur-serre
[apisprout]: https://github.com/danielgtaylor/apisprout
[ReDoc cli]: https://github.com/Rebilly/ReDoc/tree/master/cli
